<?php

/**
 * @file
 * Provides a table for store concurrent_login data.
 */

use Drupal\Component\Utility\Crypt;

/**
 * Implements hook_install().
 */
function concurrent_users_notification_install() {
  // Populate the cron key state variable.
  $cun_key = Crypt::randomBytesBase64(55);
  \Drupal::state()->set('concurrent_users_notification.cun_key', $cun_key);
}

/**
 * Implements hook_schema().
 */
function concurrent_users_notification_schema() {
  $schema['concurrent_users_notification'] = array(
    'description' => 'Stores concurrent login data.',
    'fields' => array(
      'item_id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique item ID.',
      ),
      'concurrent_logins_date' => array(
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 32,
        'description' => 'Concurrent logins date.',
      ),
      'concurrent_logins_count' => array(
        'type' => 'int',
        'not null' => TRUE,
        'description' => 'Concurrent logins count.',
      ),
    ),
    'primary key' => array('item_id'),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 *
 * It's good to clean up after ourselves.
 *
 * @ingroup concurrent_users_notification
 */
function concurrent_users_notification_uninstall() {
  db_drop_table('concurrent_users_notification');
}
